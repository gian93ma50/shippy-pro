/* eslint-disable import/prefer-default-export */
import * as R from 'ramda'

/*
Enrich the flight objects with the 
information of airports and airlines
*/
export const enrichFlights = (flights, airports, airlines) =>
  R.map((single) =>
    R.pipe(
      R.assoc(
        'arrivalAirport',
        R.find(R.propEq('id', single.arrivalAirportId))(airports)
      ),
      R.dissoc('arrivalAirportId'),
      R.assoc(
        'departureAirport',
        R.find(R.propEq('id', single.departureAirportId))(airports)
      ),
      R.dissoc('departureAirportId'),
      R.assoc('airline', R.find(R.propEq('id', single.airlineId))(airlines)),
      R.dissoc('airlineId')
    )(single)
  )(flights)
