/* eslint-disable react/prop-types */
import React from 'react'

const DisplayFlight = ({ flight }) => {
  const { airline, price, departureAirport, arrivalAirport } = flight
  const { name, codeIataPrefix = '' } = airline

  return (
    <div className="flight">
      <h2>{name}</h2>
      <p className="airports">
        <span className="airports__name">
          {departureAirport.codeIata} {codeIataPrefix}
        </span>
        <svg
          viewBox="0 0 24 11"
          xmlns="http://www.w3.org/2000/svg"
          height="100%"
          width="100%"
        >
          <path d="m23.1618494 1.26214095c-1.6504761-.88158798-3.5258375-.41328823-4.8082073.13474428l-3.8227532 1.6336838-6.57463364-3.03056903-3.91538504.1541681 5.42911193 5.04421721-3.45998247 1.50631968-3.59979471-1.25557579-2.41020497 1.02996062 2.17815617 2.47886909c-.23012206.25160415-.43865474.6058512-.24934007.97793738.24726511.48591251.97809285.73077041 2.17687167.73077041.24583241 0 .51137686-.0102779.79638634-.0309242 1.3159148-.0952628 2.74259252-.4031009 3.63467367-.78433304l13.66484342-5.83977894c1.1802525-.50443077 1.7681556-1.03408082 1.7973037-1.619331.0151175-.30353684-.1178276-.74611927-.8370455-1.13015857z" />
        </svg>
        <span className="airports__name">
          {`${arrivalAirport.codeIata} ${codeIataPrefix}`}
        </span>
      </p>
      <p className="price">{price} €</p>
    </div>
  )
}

export default DisplayFlight
