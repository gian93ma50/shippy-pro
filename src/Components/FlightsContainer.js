import React from 'react'
import * as R from 'ramda'
import { Empty } from 'antd'
import { DisplayFlight } from 'Components'

const FlightContainer = ({ flights }) => {
  if (R.isEmpty(flights)) {
    return (
      <div className="container flights-container">
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      </div>
    )
  } else {
    return (
      <>
        <div className="container flights-container">
          {R.map((single) => <DisplayFlight key={single.id} flight={single} />)(
            flights
          )}
        </div>
      </>
    )
  }
}

export default FlightContainer
