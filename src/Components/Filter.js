import React from 'react'
import * as R from 'ramda'

import { Select, Form, Button, Row, Col } from 'antd'
import { SearchOutlined } from '@ant-design/icons'

const { Option } = Select

const Filter = ({ appStates }) => {
  const { setSearchState, state } = appStates
  const { airports } = state

  const [form] = Form.useForm()

  const onFinish = (values) => {
    setSearchState((s) => ({
      from: values.departure,
      to: values.arrival,
    }))
  }

  return (
    <>
      <div className="c-filter container">
        <div className="c-filter__content">
          <Form
            form={form}
            layout="vertical"
            name="searchFlights"
            onFinish={onFinish}
          >
            <Row gutter={20} justify="space-between">
              <Col span={8}>
                <Form.Item name="departure" rules={[{ required: true }]}>
                  <Select
                    placeholder="Departure"
                    size="large"
                    style={{ marginRight: 20 }}
                  >
                    {R.map((single) => (
                      <Option key={single.id} value={single.codeIata}>
                        {single.codeIata}
                      </Option>
                    ))(airports)}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={8}>
                <Form.Item name="arrival" rules={[{ required: true }]}>
                  <Select
                    placeholder="Arrival"
                    size="large"
                    style={{ marginRight: 20 }}
                  >
                    {R.map((single) => (
                      <Option key={single.id} value={single.codeIata}>
                        {single.codeIata}
                      </Option>
                    ))(airports)}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={8}>
                <Button
                  htmlType="submit"
                  type="primary"
                  icon={<SearchOutlined />}
                  size="large"
                >
                  Find your Flights
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </div>
    </>
  )
}

export default Filter
