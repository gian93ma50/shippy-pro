import React, { useEffect, useState } from 'react'
import * as R from 'ramda'
import { getAllAirports, getAllAirlines, getFlightsFromTo } from 'API/endpoints'
import { enrichFlights } from 'Model/enrichments'
import { Header, Filter, FlightsContainer } from 'Components'

const App = () => {
  const [state, setState] = useState({
    flights: [],
    airports: [],
    airlines: [],
  })

  const [searchState, setSearchState] = useState({
    from: null,
    to: null,
  })

  useEffect(() => {
    const getAirports = async () => {
      const allAirports = await getAllAirports()
      setState((s) => ({
        ...s,
        airports: allAirports,
      }))
    }

    const getAirlines = async () => {
      const allAirlines = await getAllAirlines()
      setState((s) => ({
        ...s,
        airlines: allAirlines,
      }))
    }

    getAirports()
    getAirlines()
  }, [])

  useEffect(() => {
    const findFlights = async () => {
      if (!R.isNil(searchState.from) && !R.isNil(searchState.to)) {
        const flights = await getFlightsFromTo(searchState.from, searchState.to)
        const enrichedFlights = enrichFlights(
          flights,
          state.airports,
          state.airlines
        )
        setState((s) => ({
          ...s,
          flights: enrichedFlights,
        }))
      }
    }

    findFlights()
  }, [searchState.from, searchState.to])

  return (
    <div className="App">
      <Header />
      <Filter
        from={searchState.from}
        to={searchState.to}
        appStates={{ state, setSearchState }}
      />
      <FlightsContainer flights={state.flights} />
    </div>
  )
}

export default App
