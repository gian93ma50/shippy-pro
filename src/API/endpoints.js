import { baseUrl, token } from 'API'

const defaultOptions = {
  method: 'GET',
  mode: 'cors',
  cache: 'no-cache',
  headers: {
    Authorization: `Bearer ${token}`,
    'Content-Type': 'application/json',
  },
}

export const getAllFlights = async () => {
  const response = await (
    await fetch(`${baseUrl}flights/all`, defaultOptions)
  ).json()
  return response.data
}

export const getAllAirports = async () => {
  const response = await (
    await fetch(`${baseUrl}airports/all`, defaultOptions)
  ).json()

  return response.data
}

export const getAllAirlines = async () => {
  const response = await (
    await fetch(`${baseUrl}airlines/all`, defaultOptions)
  ).json()

  return response.data
}

export const getFlightsFromTo = async (departureCode, arrivalCode) => {
  const response = await (
    await fetch(
      `${baseUrl}flights/from/${departureCode}/to/${arrivalCode}`,
      defaultOptions
    )
  ).json()

  return response.data
}
